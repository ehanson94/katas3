const container = document.getElementById('container');

function addSomethingToPage(text) {
    const newDiv = document.createElement('div')
    newDiv.textContent = text
    container.appendChild(newDiv)
}

function oneThroughTwenty() {
    const num = []
    for (let i = 1; i <= 20; i++) {
        num.push(i)
    }
    addSomethingToPage(`Number 1: ${num}`)
}
oneThroughTwenty();

function evensThroughTwenty() {
    const num = []
    for (let i = 2; i <= 20; i += 2) {
        num.push(i)
    }
    addSomethingToPage(`Number 2: ${num}`)
}
evensThroughTwenty();

function oddsThroughTwenty() {
    const num = []
    for (let i = 1; i < 20; i += 2) {
        num.push(i)
    }
    addSomethingToPage(`Number 3: ${num}`)
}
oddsThroughTwenty();

function multiplesOfFive() {
    const num = []
    for (let i = 5; i <=100; i += 5) {
        num.push(i)
    }
    addSomethingToPage(`Number 4: ${num}`)
}
multiplesOfFive();

function squareNumbers() {
    const num = []
    for (let i = 1; i <= 100; i++) {
        let sqRoot = Math.sqrt(i)
        if (Math.floor(sqRoot) !== sqRoot) {
            continue
        }
        num.push(i)
    }
    addSomethingToPage(`Number 5: ${num}`)
}
squareNumbers();

function countingBackwards() {
    const num = []
    for (let i = 20; i > 0; i--) {
        num.push(i)
    }
    addSomethingToPage(`Number 6: ${num}`)
}
countingBackwards();

function evensBackwards() {
    const num = []
    for (let i = 20; i > 0; i -= 2) {
        num.push(i)
    }
    addSomethingToPage(`Number 7: ${num}`)
}
evensBackwards();

function oddsBackwards() {
    const num = []
    for (let i = 19; i > 0; i -= 2) {
        num.push(i)
    }
    addSomethingToPage(`Number 8: ${num}`)
}
oddsBackwards();

function multiplesOfFiveBackwards() {
    const num = []
    for (let i = 100; i > 0; i -=5) {
        num.push(i)
    }
    addSomethingToPage(`Number 9: ${num}`)
}
multiplesOfFiveBackwards();

function squareNumbersBackwards() {
    const num = []
    for (let i = 100; i > 0; i--) {
        let sqRoot = Math.sqrt(i)
        if (Math.floor(sqRoot) !== sqRoot) {
            continue
        }
        num.push(i)
    }
    addSomethingToPage(`Number 10: ${num}`)
}
squareNumbersBackwards();

const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

addSomethingToPage(`Number 11: ${sampleArray}`);

function evensInArray() {
    let arr = []
    for (let i = 0; i < sampleArray.length; i++) {
        if (sampleArray[i] % 2 === 0) {
            arr.push(sampleArray[i])
        }
    }
    addSomethingToPage(`Number 12: ${arr}`)
}
evensInArray();

function oddsInArray() {
    let arr = []
    for (let i = 0; i < sampleArray.length; i++) {
        if (sampleArray[i] % 2 !== 0) {
            arr.push(sampleArray[i])
        }
    }
    addSomethingToPage(`Number 13: ${arr}`)
}
oddsInArray();

function squareArray() {
    let num = []
    for (let i = 0; i < sampleArray.length; i++) {
        num.push(sampleArray[i] * sampleArray[i])
    }
    addSomethingToPage(`Number 14: ${num}`)
}
squareArray();

function sumArray() {
    let num = 0
    for (let i = 0; i<=20; i++) {
        num += i
    }
    addSomethingToPage(`Number 15: ${num}`)
}
sumArray();

function sumSampleArray(sampleArray) {
    let arr = 0
    for (let i = 0; i < sampleArray.length; i++) {
        arr += sampleArray[i]
    }
    addSomethingToPage(`Number 16: ${arr}`)
}
sumSampleArray(sampleArray);


function arrayMin() {
    let min = 0
    min = Math.min(...sampleArray)
    addSomethingToPage(`Number 17: ${min}`)
}
arrayMin();

function arrayMax() {
    let max = 0
    max = Math.max(...sampleArray)
    addSomethingToPage(`Number 18: ${max}`)
}
arrayMax();

function twentyRectangles() {
    for (let i = 1; i <= 20; i++) {
        let newElement = document.createElement('div')
        newElement.className = 'rectangle'
        newElement.style.width = 100 + 'px'
        let destination = document.getElementById('bar')
        destination.appendChild(newElement)
    }
}
twentyRectangles();

function moreRectangles() {
    for (let i = 105; i <= 200; i += 5) {
        let newElement = document.createElement('div')
        newElement.className = 'rectangle'
        newElement.style.width = i + 'px'
        let destination = document.getElementById('bar')
        destination.appendChild(newElement)
    }
}
moreRectangles();

function arrayRectangles() {
    for (let i = 0; i < sampleArray.length; i++) {
        let newElement = document.createElement('div')
        newElement.className = 'rectangle'
        newElement.style.width = sampleArray[i] + 'px'
        let destination = document.getElementById('bar')
        destination.appendChild(newElement)
    }
}
arrayRectangles();

function redRectangles() {
    for (let i = 0; i < sampleArray.length; i++) {
        let newElement = document.createElement('div')
        newElement.className = 'rectangle'
        newElement.style.width = sampleArray[i] + 'px'
        if (i % 2 == 0) {
            newElement.style.background = 'red'
        }
        let destination = document.getElementById('bar')
        destination.appendChild(newElement)
    }
}
redRectangles();

function evenRedRectangles() {
    for (let i = 0; i < sampleArray.length; i++) {
        let newElement = document.createElement('div')
        newElement.className = 'rectangle'
        newElement.style.width = sampleArray[i] + 'px'
        if (sampleArray[i] % 2 == 0) {
            newElement.style.background = 'red'
        }
        let destination = document.getElementById('bar')
        destination.appendChild(newElement)
    }
}
evenRedRectangles();